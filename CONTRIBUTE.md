**Contribute To This Project**
===============================
Hey there, I am working on this project my self as well but I would love others to contribute as well. You only need 
to edit the ```Twoooly.py``` file where the primary class is. Bug fixes will also be appreciated. You can add any feature that you like!

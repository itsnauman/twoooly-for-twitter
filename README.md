**Twoooly For Twitter**
===================
**Twoooly** is a Python twitter bot that automatically follows users that match a specified search query on Twitter and can unfollow users that are not following you back.If you want to help just fork the repo and send pull requests. It's up to you, add any feature that you like.
Hit me up on [Twitter](http://twitter.com/itsnauman) if you wanna ask something or just want to say hi 😃

**Disclaimer**
---------------
I hold no liability for what you do with this script or what happens to you by using this script. Abusing this script **can** get you banned from Twitter, so make sure to read up on proper usage of the Twitter API.Also make sure that you don't exceed the API requests limit of **350 requests/hour**

**Dependencies**
----------------
You will need to install Python's `tweepy` library first, it is recommende to install it inside `virtualenv`:

```easy_install tweepy```
or
```pip install tweepy```

You will also need to create an app account on https://dev.twitter.com/apps

1. Sign in with your Twitter account
2. Create a new app account
3. Modify the settings for that app account to allow read & write
4. Generate a new OAuth token with those permissions
5. Manually edit this script and put those tokens in the script

**Setup**
----------

####Running Twoooly for the first time
Open the file `setup.py` and add your Twitter information


  ```python
     API_KEY = ""
     API_SECRET  = ""
     TOKEN = ""
     TOKEN_SECRET = ""
  ```
  
**Usage**
----------
``` import Twoooly``` into your python script and make a useable instance of the class ```twoooly```. You can do this by ```app = Twoooly.twoooly()```. This will allow you call any methods from the ```twoooly``` class.Remeber Twitter limits the number of requests to **350/hour**.

**Examples**
-------------
####1. Auto-Follow all users that are following you:
This script will inform you via the console when it's done - takes time if you are following a high number of users
```python
from Twoooly import twoooly
app = twoooly()
app.auto_follow()
```
####2. Follow users who tweet about a certain hashtag:
By running this script you will be able to follow users who tweet about a specific hashtag, eg if your enter ```iOS```, you will follow people who have tweeted about it, You can limit the number of users you can follow - default is set to 10
```python
from Twoooly import twoooly
app = twoooly()
app.follow_users_with_tweets('iOS',num_of_users = 50)
```
####Cheers! 😃
![Alt Text](http://24.media.tumblr.com/tumblr_m9trixXFHn1rxlmf0o1_400.gif)

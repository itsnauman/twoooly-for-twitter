"""
Copyright 2014 Nauman Ahmad

This file is part of the Twoooly library.

Twoooly is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your option) any
later version.

Twoooly is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Twoooly.
If not, see http://www.gnu.org/licenses/.
"""
import tweepy
import SETUP

class twoooly(object):

    def __init__(self):
        """
        Creates an instance of the API class
        Authenticates the user using the OAuthHandler method with the Twitter API
        """
        self.API_KEY = setup.API_KEY
        self.API_SECRET = setup.API_SECRET
        self.TOKEN_SECRET = setup.TOKEN_SECRET
        self.TOKEN = setup.TOKEN
        try:
            self.auth = tweepy.OAuthHandler(self.API_KEY,self.API_SECRET)
            self.auth.set_access_token(self.TOKEN,self.TOKEN_SECRET)
            self.twitter = tweepy.API(self.auth)
        except tweepy.TweepError as e:
            print 'An Error has occured wile authenticating \n'
            raise e
        self.me = self.twitter.me()
        self.name = self.me.name

    def auto_follow(self):
        """
        Follow back everyone who has follows you
        """
        self.followers = self.twitter.followers_ids(self.name)
        self.following = self.twitter.friends_ids(self.name)
        print 'Setting up...'
        print 'Please be patient' % (self.name)
        try:
            for each_follower in self.followers:
                if each_follower not in self.following:
                    self.twitter.create_friendship(each_follower)
                    print "-" * 30
                    print "Following The User.."
                else:
                    print "-" * 30
                    print "You Are Already Following Him!"
        except tweepy.TweepError as e:
            print 'Error \n'
            raise e

    def auto_unfollow(self):
        """
        Unfollows the users who are not following you back
        """
        self.followers = self.twitter.followers_ids(self.name)
        self.following = self.twitter.friends_ids(self.name)
        print 'Setting up....'
        print 'Please wait %s' % (self.name)
        try:

            for each_friend in self.following:
                if each_friend not in self.followers:
                    self.twitter.destroy_friendship(each_friend)
                    print "-" * 30
                    print "Unfollowed The User.."
                else:
                    print "-" * 30
                    print "He is following you.."
        except tweepy.TweepError as e:
            print 'Error has occured... \n'
            raise e

    def clear_timeline(self,count = 100):
        """
        Delete tweets tweeted by the authenticated user
        count param specifies the number of tweets to delete
        """

        print "Getting all tweets....."
        self.timeline_tweets = self.twitter.user_timeline(count = count)
        print "Found: %d" % (len(self.timeline_tweets))
        print "Removing, please wait %s..." % (self.name)
        try:
            for t in self.timeline_tweets:
                self.twitter.destroy_status(t.id)
            print "Done"
        except tweepy.TweepError as e:
            print 'An error has occured \n'
            raise e

    def favorite_tweets(self,search,num_of_tweets = 10):
        """
        Favorite tweets about a certain hashtag
        search param specifies the phrase to search
        num_of_tweets param specifies the number of tweets to favorite
        """
        search = '#'+search
        try:
            cursor = tweepy.Cursor(self.twitter.search,q=search,lang='en').items(int(num_of_tweets))
            print 'Please wait %s while the tweets related to %s are fetched......' % (self.name,search)
            print 'It might take some time....'
            for each_tweet in cursor:
                self.twitter.create_favorite(each_tweet.id)
            print 'Done, go check it out...'
        except tweepy.TweepError as e:
            print 'Error... \n'
            raise e

    def retweet_tweets(self,search,num_of_tweets = 10):
        """
        Retweet tweets about a certain hashtag
        search param specifies the phrase to search
        num_of_tweets param specifies the number of tweets to retweet
        """
        search = '#'+search
        try:
            cursor = tweepy.Cursor(self.twitter.search, q=search, lang='en').items(int(num_of_tweets))
            print 'Please wait %s while the tweets related to %s are fetched......' % (self.name,search)
            print 'It might take some time....'
            for each_tweet in cursor:
                self.twitter.retweet(each_tweet.id)
            print 'Done, go check it out...'
        except tweepy.TweepError as e:
            print 'Error has occured... \n'
            raise e

    def follow_user_with_tweets(self,search,num_of_users = 10):
        """
        Follow users who tweet about a specific hashtags
        search param specifies the phrase to search
        num_of_users param specifies the number of users to follow
        """
        search = '#'+search
        try:
            self.following = self.twitter.friends_ids(self.name)
            print 'Getting Tweets Related To %s' % (search)
            cursor = tweepy.Cursor(self.twitter.search,q=search,lang='en').items(int(num_of_users))
            print 'Please be patient %s.....' % (self.name)
            for each_tweet in cursor:
                if each_tweet.user.screen_name not in self.following:
                    self.twitter.create_friendship(each_tweet.user.screen_name)
                    print "-" * 35
                    print 'Followed %s' % (each_tweet.user.screen_name)
                else:
                    print "-" * 35
                    print 'You Are Already Following %s' % (each_tweet.user.screen_name)
            print "-" * 35
            print 'Done Following!'
        except tweepy.TweepError as e:
            print 'Error has occured following users...'
            raise e

    def delete_recieved_messages(self):
        """
        Delete all messages sent by other users to clean your inbox
        """
        print 'Getting messages....'
        msg = self.twitter.direct_messages()
        print 'Please wait while the messages a deleted....'
        try:
            for each in msg:
                self.twitter.destroy_direct_message(each.id)
            print 'Done!'
        except tweepy.TweepError as e:
            print 'An Error has occured \n'
            raise e

    def change_avatar(self,file_path):
        """
        Changes the profile avatar of the authenticated profile
        Valid Formats: GIF, JPG, or PNG
        """
        try:
            self.twitter.update_profile_image(file_path)
            print "'Profile image of %s 's profile  changed" % (self.name)
        except tweepy.TweepError as e:
            raise e

    def change_background(self,file_path):
        """
        Changes the background image of the authenticated profile
        Valid Formats: GIF, JPG, or PNG
        """
        try:
            self.twitter.update_profile_background_image(file_path)
            print "Background Image Changed"
        except tweepy.TweepError as e:
            raise e

    def unfavourite_tweets(self,number = 50):
        """
        Unfavorite all tweets
        number param specifies the number of tweets to iterate through
        if number = 0 all tweets are iterated
        """
        print 'Loading Tweets.....'
        fav_tweets = self.twitter.favorites(self.name)
        print 'Please wait....'
        try:
            if number == 0:
                for each_tweets in fav_tweets:
                    self.twitter.destroy_favorite(each_tweets.id)
            else:
                counter = 0
                for each_tweets in fav_tweets:
                    if counter == number:
                        break
                    else:
                        self.twitter.destroy_favorite(each_tweets.id)
                    counter += 1
            print 'Done!'
        except tweepy.TweepError as e:
            raise e

"""
Copyright 2014 Nauman Ahmad

This file is part of the Twoooly library.

Twoooly is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your option) any
later version.

Twoooly is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Twoooly.
If not, see http://www.gnu.org/licenses/.
"""
import Twoooly

API_KEY = "ENTER KEY HERE"
API_SECRET  ="ENTER SECRET KEY HERE"
TOKEN = "ENTER TOKEN HERE"
TOKEN_SECRET = "ENTER SECRET TOKEN HERE"
